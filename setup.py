from settings import MASTODON_APP_NAME

from mastodon import Mastodon

def register(api_base_url):
    print("Registering application...")

    client_id, client_secret = Mastodon.create_app(
        MASTODON_APP_NAME,
        api_base_url = api_base_url,
    )

    return client_id, client_secret

def log_in(api_base_url, client_id, client_secret, email, password):
    print("Logging in...")

    mastodon = Mastodon(
        client_id = client_id,
        client_secret = client_secret,
        api_base_url = api_base_url)

    user_secret = mastodon.log_in(
        email,
        password)

    return user_secret

def input_credentials():
    print(
        "Input the email and password of a Mastodon account "
        "registered in the instance above. It will be used to "
        "log in to that account with the application."
    )
    print()
    
    email = input("Email: ")
    password = input("Password: ")

    print()

    return email, password

def input_instance():
    default = "https://botsin.space"
    print(
        "Input the base URL of the Mastodon instance. It will "
        "be used to register a new application in the "
        "instance. "
        f'If empty, defaults to "{default}".')
    print()

    api_base_url = input("Mastodon base URL: ")
    if not api_base_url:
        api_base_url = default

    print()

    return api_base_url

def print_secret(user_secret, api_base_url):
    print()
    print(
        f'The user secret is: "{user_secret}". '
        "You can paste the following environment "
        "variable declarations to the `.env` file:")
    print()
    print(f"MASTODON_API_BASE_URL={api_base_url}")
    print(f"MASTODON_USER_SECRET={user_secret}")
    print()

def setup():
    api_base_url = input_instance()
    email, password = input_credentials()

    client_id, client_secret = register(api_base_url)
    user_secret = log_in(api_base_url, client_id, client_secret, email, password)
    
    print_secret(user_secret, api_base_url)

if __name__ == "__main__":
    setup()