from dotenv import load_dotenv
load_dotenv(override=True)

import logging

log = logging
log.basicConfig(level=logging.INFO)

from os import getenv

MASTODON_APP_NAME = "Towncrier"
MASTODON_API_BASE_URL = getenv("MASTODON_API_BASE_URL")
MASTODON_USER_SECRET = getenv("MASTODON_USER_SECRET")
LOOMIO_BASE_URL = getenv("LOOMIO_BASE_URL")
LOOMIO_API_BASE_URL = f'{LOOMIO_BASE_URL}/api'
LOOMIO_API_PAGE_SIZE = 50

FOREVER_MINUTES = 20