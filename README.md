Towncrier
====

**Towncrier** is a Mastodon bot that relays activity from a
Loomio instance.

Contributions welcome! Licensed under the WTFPLv2.

What does it do?
----

When run, this bot will fetch all public discussions and polls
from a given Loomio instance, and send a new toot to the given
Mastodon account for each discussion or poll created since it
last tooted.

Installation
----

To run this bot in your local machine, you will need Python 3.6.
You will also need Pip to install the dependencies.

Run `pip install -r requirements.txt` to install the
dependencies.

Set up
----

Copy the existing `.env.sample` file to a new `.env` file.

Modify the `.env` file, adding the necessary values to each of
the following keys:

* `LOOMIO_BASE_URL`: The user-facing URL of the Loomio instance whose
activity you wish to relay.

Run `python setup.py` to set up the bot for a given Mastodon account.
It will prompt for a Mastodon instance's base URL, as well
as for the username and password of an account registered in that
instance. It will output two environment variable declarations. Replace
the corresponding declarations in the `.env` file with those obtained
from the setup.

Run
----

Run `python run.py` to run the bot once, tooting all the activity updates
that took place since the last toot, or run `python forever.py` to keep
running the bot forever, polling for updates periodically.

Debug
----

Run `python ping.py` to toot to the Mastodon account. You can then run
`python reset.py` to clear all recent toots in the account.