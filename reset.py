from ping import ping

from settings import log

from tooter import client, last_toots

def clear():
    toots = last_toots()
    for toot in toots:
        client.status_delete(toot)
    
    log.info(f"Successfully deleted last {len(toots)} toots")

if __name__ == "__main__":
    clear()
    ping()