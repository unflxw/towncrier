from tooter import client

from settings import log

def ping():
    client.toot("Toot!")

if __name__ == "__main__":
    ping()
    log.info("Pong!")