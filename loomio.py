from dateutil.parser import parse

from settings import (
    LOOMIO_API_BASE_URL,
    LOOMIO_BASE_URL,
    LOOMIO_API_PAGE_SIZE
)

import requests

def element_page(page, url, key):
    payload = {
        "from": page * LOOMIO_API_PAGE_SIZE,
        "per": LOOMIO_API_PAGE_SIZE
    }

    response = requests.get(url, params=payload)
    return response.json()[key]

def element_pages(url, key):
    page = 0
    elements = []

    while True:
        new_elements = element_page(page, url, key)
        elements = elements + new_elements
        
        page = page + 1
        if len(new_elements) < LOOMIO_API_PAGE_SIZE:
            return elements

def discussions():
    url = f"{LOOMIO_API_BASE_URL}/v1/discussions.json"
    return element_pages(url, 'discussions')

def polls():
    url = f"{LOOMIO_API_BASE_URL}/v1/polls.json"
    return element_pages(url, 'polls')

def elements_since(elements, since):
    return [element for element in elements
            if element_created_at(element) > since]

def polls_since(since):
    return elements_since(polls(), since)

def discussions_since(since):
    return elements_since(discussions(), since)

def element_created_at(poll):
    return parse(poll['created_at'], yearfirst=True)

def poll_view_url(poll):
    return f"{LOOMIO_BASE_URL}/p/{poll['key']}"

def discussion_view_url(discussion):
    return f"{LOOMIO_BASE_URL}/d/{discussion['key']}"