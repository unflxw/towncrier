from run import run

from time import sleep

from settings import log
from settings import FOREVER_MINUTES as minutes

def forever():
    while True:
        success = run()
        if not success:
            os.exit(1)
        
        log.info(f"Sleeping for {minutes} minutes...")
        sleep(minutes * 60)

if __name__ == "__main__":
    forever()