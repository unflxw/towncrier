from settings import (
    MASTODON_API_BASE_URL,
    MASTODON_USER_SECRET
)

from mastodon import Mastodon

client = Mastodon(
    access_token = MASTODON_USER_SECRET,
    api_base_url = MASTODON_API_BASE_URL
)

def last_toots():
    account = client.account_verify_credentials()

    return client.account_statuses(account)

def last_toot_at():
    toots = last_toots()

    if toots:
        last_toot = toots[0]
        return last_toot.created_at