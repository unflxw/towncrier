from tooter import client, last_toot_at
import loomio

from settings import log

def poll_toot(poll):
    return "\n".join([
        f'New {poll["poll_type"]} created: "{poll["title"]}"',
        loomio.poll_view_url(poll)
    ])

def discussion_toot(discussion):
    return "\n".join([
        f'New discussion created: "{discussion["title"]}"',
        loomio.discussion_view_url(discussion)
    ])

def run():
    since = last_toot_at()

    if not since:
        log.error("No previous toot exists.")
        return False

    polls = loomio.polls_since(since)
    log.info(f"Found {len(polls)} polls since the last toot")

    poll_toots = [poll_toot(poll) for poll in polls]
    
    discussions = loomio.discussions_since(since)
    log.info(f"Found {len(discussions)} discussions since the last toot")

    discussion_toots = [discussion_toot(discussion)
                        for discussion in discussions]

    toots = poll_toots + discussion_toots

    for toot in toots:
        client.toot(toot)
        log.debug(f"Tooted {repr(toot)}")
    
    log.info(f"Tooted {len(toots)} toots")

    return True

if __name__ == "__main__":
    success = run()
    if not success:
        os.exit(1)
